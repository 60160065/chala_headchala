/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author Windows 10
 */
/*POJO*/
public class Employees {
   int empId;
   String name;
   String surname;
   String phone;
   String cardId;
   int type;
   String username;
   String password;

   public Employees() {
        this.empId=-1;
    }

    public Employees(int empId, String name, String surname, String phone, String cardId, int type, String username, String password) {
        this.empId = empId;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.cardId = cardId;
        this.type = type;
        this.username = username;
        this.password = password;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }
    

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Employees{" + "empId=" + empId + ", name=" + name + ", surname=" + surname + ", phone=" + phone + ", cardId=" + cardId + ", type=" + type + ", username=" + username + ", password=" + password + '}';
    }

   }
