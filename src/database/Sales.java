/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author informatics
 */
public class Sales {
    int salesId;
    String date;
    String name;
    String topping;
    int amount;
    double price;
    int amountTopping;
    double priceTopping;
    double total;
    
    public Sales(){
        this.salesId=-1;
    }

    public Sales(int salesId, String date, String name, String topping, int amount, double price, int amountTopping, double priceTopping, double total) {
        this.salesId = salesId;
        this.date = date;
        this.name = name;
        this.topping = topping;
        this.amount = amount;
        this.price = price;
        this.amountTopping = amountTopping;
        this.priceTopping = priceTopping;
        this.total = total;
    }

    public int getSalesId() {
        return salesId;
    }

    public void setSalesId(int salesId) {
        this.salesId = salesId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

        public String getTopping() {
        return topping;
    }

    public void setTopping(String topping) {
        this.topping = topping;
    }

    public int getAmountTopping() {
        return amountTopping;
    }

    public void setAmountTopping(int amountTopping) {
        this.amountTopping = amountTopping;
    }

    public double getPriceTopping() {
        return priceTopping;
    }

    public void setPriceTopping(double priceTopping) {
        this.priceTopping = priceTopping;
    }

    @Override
    public String toString() {
        return "Sales{" + "salesId=" + salesId + ", date=" + date + ", name=" + name + ", topping=" + topping + ", amount=" + amount + ", price=" + price + ", amountTopping=" + amountTopping + ", priceTopping=" + priceTopping + ", total=" + total + '}';
    } 
    
}