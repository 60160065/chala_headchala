/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import chala.CHALA;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author natth
 */
public class CustomerDao {
    
   private static Object[] cusId;
   
    
    
    public static ArrayList<Customer> getCus() {
      ArrayList<Customer> customer = new ArrayList();
        Connection conn = Database.connect(); 
         try {
            Statement stm = conn.createStatement();
            String sql = "SELECT *\n" +
                          "  FROM customer";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Customer cus = toObject(rs);
                customer.add(cus);
            }
            Database.close();
            return customer;
        } catch (SQLException ex) {
            Logger.getLogger(CHALA.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
    public static Customer toObject(ResultSet rs) throws SQLException {
        Customer customer;
        customer = new Customer();
        customer.setCusId(rs.getInt("cusId"));
        customer.setName(rs.getString("name"));
        customer.setSurname(rs.getString("surname"));
        customer.setPhone(rs.getString("phone"));
        customer.setPoint(rs.getInt("score"));
        return customer;
        
        
    }
        public static Customer getCustomer(int cusId) {
        String sql = "SELECT * FROM customer WHERE cusId = %d";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, cusId));
            if(rs.next()){
                Customer customer = toObject(rs);
                Database.close();
                return customer;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
    return null;
    
    }
        public static boolean insert(Customer cus) {
        Connection conn = Database.connect();        
        try {Statement stm = conn.createStatement();
        String sql = "INSERT INTO customer (\n" +
"                         name,\n" +
"                         surname,\n" +
"                         phone,\n" +
"                         score\n" +                       
"                     )\n" +

"                     VALUES (\n" +
        
"                         '%s',\n" +
"                         '%s',\n" +
"                         '%s',\n" +
"                         %d\n" +          
"                     );";
           stm.execute(String.format(sql,cus.getName(),cus.getSurname()
                   ,cus.getPhone(),cus.getPoint()));
           Database.close();
           return true;
       } catch (SQLException ex) {
           Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
       }
        Database.close();
        return true;
    }
public static boolean update(Customer cus) {
     String sql = "UPDATE customer\n" +
    "   SET name = '%s',\n" +
    "       surname = '%s',\n" +
    "       phone = '%s',\n" +
    "       score = '%d'\n" +
    " WHERE cusId = %d ;";
        Connection conn = Database.connect();
        
         
       try {
         Statement stm = conn.createStatement();
         boolean  ret = stm.execute(String.format(sql,cus.getName(),cus.getSurname(),cus.getPhone()
                 ,cus.getPoint(),cus.getCusId()));
         Database.close();
          return ret;
       } catch (SQLException ex) {
           Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
       }
            Database.close();
            return false;
    }
public static boolean delete(Customer cus) {
        String sql = "DELETE FROM customer WHERE cusId = %d;";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, cus.getCusId()));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }
public static void save(Customer cus) {
        if (cus.getCusId() < 0) {
            insert(cus);
        } else {
            update(cus);
        }
        
}
}

