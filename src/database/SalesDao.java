/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import chala.CHALA;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class SalesDao {

    public static ArrayList<Sales> getSales() {
        ArrayList<Sales> sales = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT billId,\n"
                    + "       dateTime,\n"
                    + "       orderDetail.drink,\n"
                    + "       orderDetail.topping,\n"
                    + "       orderDetail.amount,\n"
                    + "       orderDetail.price,\n"
                    + "       orderDetail.amountTopping,\n"
                    + "       orderDetail.priceTopping,\n"
                    + "       orderDetail.total\n"
                    + "  FROM bill,ordering,orderDetail\n"
                    + " WHERE bill.orderingId = ordering.orderingId\n"
                    + " AND ordering.orderingId = 1;";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Sales sale = toObject(rs);
                sales.add(sale);
            }
            Database.close();
            return sales;
        } catch (SQLException ex) {
            Logger.getLogger(CHALA.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    public static Sales toObject(ResultSet rs) throws SQLException {
        Sales sales;
        sales = new Sales();
        sales.setSalesId(rs.getInt("billId"));
        sales.setDate(rs.getString("dateTime"));
        sales.setName(rs.getString("drink"));
        sales.setTopping(rs.getString("topping"));
        sales.setAmount(rs.getInt("amount"));
        sales.setPrice(rs.getInt("price"));
        sales.setAmountTopping(rs.getInt("amountTopping"));
        sales.setPriceTopping(rs.getInt("priceTopping"));
        sales.setTotal(rs.getDouble("total"));
        return sales;

    }
}
