/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class Addnewmenu {
    int typeProductId;
    String name;
    String type;
    double price;
    int productId;

    public Addnewmenu() {
        this.typeProductId = -1;
    }

    public Addnewmenu(int typeProductId, String name, String type, double price, int productId) {
        this.typeProductId = typeProductId;
        this.name = name;
        this.type = type;
        this.price = price;
        this.productId = productId;
    }

    public int getTypeProductId() {
        return typeProductId;
    }

    public void setTypeProductId(int typeProductId) {
        this.typeProductId = typeProductId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "AddNewMenu{" + "typeProductId=" + typeProductId + ", name=" + name + ", type=" + type + ", price=" + price + ", productId=" + productId + '}';
    }
    
    
    
   
}

