/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author PC-Rom
 */
public class OrderingVendor {
   int vendorId;
   int productId;
   String name;
   int amount;
   Double price;
   Double total;
   String type;
   String phone;

   
   
   public OrderingVendor() {
        this.productId = -1;
    }
   
    public OrderingVendor(int vendorId,int productId, String name, int amount, Double price, Double total, String type, String phone) {
        this.vendorId = vendorId;
        this.productId = productId;
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.total = total;
        this.type = type;
        this.phone = phone;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    
    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "OrderingVendor{" + "vendorId=" + vendorId +"productId=" + productId + ", name=" + name + ", amount=" + amount + ", price=" + price + ", total=" + total + ", type=" + type + ", phone=" + phone + '}';
    }

   
   
    
}
