/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import chala.CHALA;
import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PC-Rom
 */
public class OrderingVendorDao {

    public static ArrayList<OrderingVendor> getOrVen() {
        ArrayList<OrderingVendor> orven = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT product.vendorId,\n"
                    + "                          productId,\n"
                    + "                         product.name,\n"
                    + "                         amount,\n"
                    + "                         price,\n"
                    + "                          total,\n"
                    + "                          vendor.type,\n"
                    + "                          vendor.phone\n"
                    + "                      FROM product,vendor\n"
                    + "                      WHERE product.vendorId = vendor.vendorId";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                OrderingVendor orv = toObject(rs);
                orven.add(orv);
            }
            Database.close();
            return orven;
        } catch (SQLException ex) {
            Logger.getLogger(CHALA.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
//INSERT INTO product (
    //                       name,
    //                       amount,
    //                       price,
    //                     total,
    //                   vendorId
    //             )
    //           VALUES (
    //             'ผงชาเขียว',
    //           100,
    //         20,
    //       2000
    // );

//INSERT INTO vendor (
    //                      phone,
    //                  type
    //             )
    //              VALUES (
    //                'ผงชาเขียว',
    //              100,
    //            20,
    //          2000
    //    );
    public static OrderingVendor toObject(ResultSet rs) throws SQLException {
        OrderingVendor orven;
        orven = new OrderingVendor();
        orven.setVendorId(rs.getInt("vendorId"));
        orven.setProductId(rs.getInt("productId"));
        orven.setName(rs.getString("name"));
        orven.setAmount(rs.getInt("amount"));
        orven.setPrice(rs.getDouble("price"));
        orven.setTotal(rs.getDouble("total"));
        orven.setType(rs.getString("type"));
        orven.setPhone(rs.getString("phone"));
        return orven;
    }

    public static OrderingVendor getOrderingVendor(int productId) {
        String sql = "SELECT product.vendorId,\n"
                + "       productId,\n"
                + "       product.name,\n"
                + "       amount,\n"
                + "       price,\n"
                + "       total,\n"
                + "       vendor.type,\n"
                + "       vendor.phone\n"
                + "  FROM product,vendor\n"
                + "WHERE product.vendorId = vendor.vendorId AND product.productId = %d;";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, productId));
            if (rs.next()) {
                OrderingVendor orv = toObject(rs);
                Database.close();
                return orv;
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderingVendorDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    public static boolean delete(OrderingVendor orven) {
        String sql = "DELETE FROM product WHERE productId = %d;";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, orven.getProductId()));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(OrderingVendorDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }

    public static boolean insert(OrderingVendor orven) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO product (\n"
                    + "                        vendorId,\n"
                    + "                        name,\n"
                    + "                        amount,\n"
                    + "                        price,\n"
                    + "                        total\n"
                    + "                    )\n"
                    + "                    VALUES ("
                    + "                        %d,"
                    + "                        '%s',"
                    + "                        %d,"
                    + "                        %f,"
                    + "                        %f"
                    + "                    );" + "INSERT INTO vendor (\n"
                    + "                        type,\n"
                    + "                        phone\n"
                    + "                    )\n"
                    + "                    VALUES ("
                    + "                        '%s',"
                    + "                        '%s'"
                    + "                    );";
            System.out.println(String.format(sql, orven.getVendorId(), orven.getName(), orven.getAmount(), orven.getPrice(), orven.getTotal(), orven.getType(), orven.getPhone()));
            stm.execute(String.format(sql,orven.getVendorId(), orven.getName(), orven.getAmount(), orven.getPrice(), orven.getTotal(), orven.getType(), orven.getPhone()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(OrderingVendorDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }

    public static boolean update(OrderingVendor orderingvendor) {
        String sql = "UPDATE product\n"
                + "   SET vendorId = '%d',\n"
                + "    name = '%s',\n"
                + "       amount = '%d',\n"
                + "       price = '%f',\n"
                + "       total = '%f'\n"
                + " WHERE productId = '%d';";
        Connection conn = Database.connect();

        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, orderingvendor.getVendorId(), orderingvendor.getName(), orderingvendor.getAmount(), orderingvendor.getPrice(), orderingvendor.getTotal(), orderingvendor.getProductId()));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(OrderingVendorDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        sql = "UPDATE vendor\n"
                + "   SET type = '%s',\n"
                + "       phone = '%s'\n"
                + " WHERE vendorId = '%d';";
        Connection conn1 = Database.connect();

        try {
            Statement stm1 = conn1.createStatement();
            boolean ret1 = stm1.execute(String.format(sql, orderingvendor.getType(), orderingvendor.getPhone(), orderingvendor.getVendorId()));
            Database.close();
            return ret1;
        } catch (SQLException ex) {
            Logger.getLogger(OrderingVendorDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return false;
    }

    public static void save(OrderingVendor orderingvendor) {
        if (orderingvendor.getProductId() < 0) {
            insert(orderingvendor);
        } else {
            update(orderingvendor);
        }
    }

}
