/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import chala.CHALA;
import static java.awt.image.ImageObserver.WIDTH;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author informatics
 */
public class VendorDao {

    public static boolean insert(Vendor vendor) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO vendor (\n"
                    + "                     name,\n"
                    + "                     phone,\n"
                    + "                     address,\n"
                    + "                     type\n"
                    + "                 )\n"
                    + "                 VALUES (\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%s'\n"
                    + "                 );";
            stm.execute(String.format(sql,
                    vendor.getName(),
                    vendor.getPhone(),
                    vendor.getAddress(),
                    vendor.getType()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(VendorDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }

    public static boolean update(Vendor vendor) {
        String sql = "UPDATE vendor \n"
                + "   SET name = '%s',\n"
                + "       phone = '%s',\n"
                + "       address = '%s',\n"
                + "       type = '%s'\n"
                + " WHERE vendorId = %d;";
        Connection conn = Database.connect();

        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql,
                    vendor.getName(),
                    vendor.getPhone(),
                    vendor.getAddress(),
                    vendor.getType(),
                    vendor.getVendorId()
            ));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(VendorDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return false;
    }

    public static boolean delete(Vendor vendor) {
        String sql = "DELETE FROM vendor WHERE vendorId = %d";
        Connection conn = Database.connect();

        try {
            Statement stm = conn.createStatement();
            stm.execute(String.format(sql, vendor.getVendorId()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(VendorDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return false;
    }

    public static ArrayList<Vendor> getVen() {
        ArrayList<Vendor> vendor = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT *\n"
                    + "  FROM vendor";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Vendor ven = toObject(rs);
                vendor.add(ven);
            }
            Database.close();
            return vendor;
        } catch (SQLException ex) {
            Logger.getLogger(CHALA.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    public static Vendor toObject(ResultSet rs) throws SQLException {
        Vendor vendor;
        vendor = new Vendor();
        vendor.setVendorId(rs.getInt("vendorId"));
        vendor.setName(rs.getString("name"));
        vendor.setPhone(rs.getString("phone"));
        vendor.setAddress(rs.getString("address"));
        vendor.setType(rs.getString("type"));
        return vendor;
    }

    public static Vendor getVendor(int venId) {
        String sql = "SELECT * FROM vendor WHERE vendorId = %d";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, venId));
            if (rs.next()) {
                Vendor ven = toObject(rs);
                Database.close();
                return ven;
            }
        } catch (SQLException ex) {
            Logger.getLogger(VendorDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    public static void save(Vendor ven) {
        if (ven.getVendorId() < 0) {
            insert(ven);
        } else {
            update(ven);
        }
    }

    public static Vendor Search(String phone) {
        String sql = "SELECT * FROM vendor WHERE phone = '%s'";
        Connection conn = database.Database.connect();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql));
            if (rs.next()) {
                Vendor ven = toObject(rs);
                Database.close();
                return ven;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Database.close();
        return null;
    }

    public static ArrayList<Vendor> Search() {
        ArrayList<Vendor> vendor2 = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT vendorId,\n"
                    + "       name,\n"
                    + "       phone,\n"
                    + "       address,\n"
                    + "       type\n"
                    + "  FROM vendor\n"
                    + " WHERE phone = '%s'";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Vendor ven = toObject(rs);
                vendor2.add(ven);
            }
            Database.close();
            return vendor2;
        } catch (SQLException ex) {
            Logger.getLogger(CHALA.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
}
