/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import chala.CHALA;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author informatics
 */
public class AddnewmenuDao {
    private static Object[] typeProductId;

    public static boolean insert(Addnewmenu addnewmenu) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO typeProduct (\n" +
                            "		name,\n"
                    + "		type,\n"
                    + "		price\n"
                    + "         )\n"
                    + "         VALUES (\n"
                    + "		'%s',\n"
                    + "		'%s',\n"
                    + "		%f\n"
                    + "          );";
            stm.execute(String.format(sql, addnewmenu.getName(), addnewmenu.getType(), addnewmenu.getPrice()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(AddnewmenuDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }

    public static boolean update(Addnewmenu addnewmenu) {
        String sql = "UPDATE typeProduct\n" +
        "   SET	name = '%s',\n"
                + "	type = '%s',\n"
                + "	price = '%f'\n"
                + " WHERE typeProductId = %d;";
        Connection conn = Database.connect();

        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, addnewmenu.getName(), addnewmenu.getType(), addnewmenu.getPrice(), addnewmenu.getTypeProductId()));
            Database.close();
            return ret ;
        } catch (SQLException ex) {
            Logger.getLogger(AddnewmenuDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return false;
    }

    public static boolean delete(Addnewmenu addnewmenu) {
        String sql = "DELETE FROM typeProduct WHERE typeProductId = %d";
        Connection conn = Database.connect();

        try {
            Statement stm = conn.createStatement();
            boolean ret =stm.execute(String.format(sql, addnewmenu.getTypeProductId()));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(AddnewmenuDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return false;
    }

    public static ArrayList<Addnewmenu> getPro() {
        ArrayList<Addnewmenu> addnewmenu = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT *\n"
                    + "  FROM typeProduct";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Addnewmenu pro = toObject(rs);
                addnewmenu.add(pro);
            }
            Database.close();
            return addnewmenu;
        } catch (SQLException ex) {
            Logger.getLogger(AddnewmenuDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static Addnewmenu toObject(ResultSet rs) throws SQLException {
        Addnewmenu addnewmenu;
        addnewmenu = new Addnewmenu();
        addnewmenu.setTypeProductId(rs.getInt("typeproductId"));
        addnewmenu.setName(rs.getString("name"));
        addnewmenu.setType(rs.getString("type"));
        addnewmenu.setPrice(rs.getDouble("price"));
        return addnewmenu;
    }

    public static Addnewmenu getTypeProductId(int typeProductId) {
        String sql = "SELECT * FROM typeProduct WHERE typeProductId = %d";
        Connection conn = Database.connect();
        Statement stm;
        try {
            stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, typeProductId));
            if (rs.next()) {
                Addnewmenu addnewmenu = toObject(rs);
                Database.close();
                return addnewmenu;
            }

        } catch (SQLException ex) {
            Logger.getLogger(AddnewmenuDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return null;
    }
public static void save(Addnewmenu pro) {
        if (pro.getTypeProductId()<0) {
            insert(pro);
        } else {
            update(pro);
        }
        
}

}
