/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import chala.CHALA;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Teddybank
 */
public class StockDao {

    private static Object[] productId;

    public static ArrayList<Stock> getStock() {
        ArrayList<Stock> stock = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT productId,\n"
                    + "       name,\n"
                    + "       storehouse,\n"
                    + "       amount,\n"
                    + "       price,\n"
                    + "       total\n"
                    + "  FROM product;";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Stock pro = toObject(rs);
                stock.add(pro);
            }
            Database.close();
            return stock;
        } catch (SQLException ex) {
            Logger.getLogger(CHALA.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    public static Stock toObject(ResultSet rs) throws SQLException {
        Stock stock;
        stock = new Stock();
        stock.setProductId(rs.getInt("productId"));
        stock.setName(rs.getString("name"));
        stock.setStorehouse(rs.getString("storehouse"));
        stock.setAmount(rs.getInt("amount"));
        stock.setPrice(rs.getDouble("price"));
        stock.setTotal(rs.getDouble("total"));

        return stock;
    }

    public static Stock getStock(int productId) {
        String sql = "SELECT * FROM product WHERE productId = %d";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, productId));
            if (rs.next()) {
                Stock stock = toObject(rs);
                Database.close();
                return stock;
            }
        } catch (SQLException ex) {
            Logger.getLogger(StockDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    public static boolean insert(Stock stock) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO product (\n"
                    + "                            name,\n"
                    + "                            storehouse,\n"
                    + "                            amount,\n"
                    + "                            price,\n"
                    + "                            total\n"
                    + "                        )\n"
                    + "                        VALUES ("
                    + "                            '%s',"
                    + "                            '%s',"
                    + "                            %d,"
                    + "                            %f,"
                    + "                            %f"
                    + "                        );";

            stm.execute(String.format(sql, stock.getName(), stock.getStorehouse(), stock.getAmount(), stock.getPrice(), stock.getTotal()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(StockDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return true;
    }

    public static boolean update(Stock stock) {
        String sql = "UPDATE product\n"
                + "   SET name = '%s',\n"
                + "       storehouse = '%s',\n"
                + "       amount = '%d',\n"
                + "       price = '%f',\n"
                + "       total = '%f'\n"
                + " WHERE productId = '%d';";
        Connection conn = Database.connect();

        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, stock.getName(), stock.getStorehouse(), stock.getAmount(), stock.getPrice(), stock.getTotal(), stock.getProductId()));

            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(StockDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return false;
    }

    public static boolean delete(Stock stock) {
        String sql = "DELETE FROM product WHERE productId = %d;";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, stock.getProductId()));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(StockDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }

    public static void save(Stock stock) {
        if (stock.getProductId() < 0) {
            insert(stock);
        } else {
            update(stock);
        }
    }
}
