/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import chala.CHALA;
import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class BillDao {
    
        public static ArrayList<Bill> getBill() {
        ArrayList<Bill> lp = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT drink,\n"
                    + "       topping,\n"
                    + "       amount,\n"
                    + "       price,\n"
                    + "       amountTopping,\n"
                    + "       priceTopping,\n"
                    + "       total\n"
                    + "  FROM orderDetail;";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Bill bill = toObject(rs);
                lp.add(bill);
            }
            Database.close();
            return lp;
        } catch (SQLException ex) {
            Logger.getLogger(CHALA.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static Bill toObject(ResultSet rs) throws SQLException {
        Bill bill;
        bill = new Bill();
        bill.setDrink(rs.getString("drink"));
        bill.setTopping(rs.getString("topping"));
        bill.setAmount(rs.getInt("amount"));
        bill.setPrice(rs.getDouble("price"));
        bill.setAmountTopping(rs.getInt("amountTopping"));
        bill.setPriceTopping(rs.getDouble("priceTopping"));
        bill.setTotal(rs.getDouble("total"));
        return bill;
    }
}
