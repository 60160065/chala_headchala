/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author informatics
 */
public class Customer {

    

     int cusId;
    String name;
    String surname;
    String phone;
    int point;

   public   Customer() {
        this.cusId=-1;
    }
    
    public Customer(int cusId, String name, String surname, String phone, int point) {
        this.cusId = cusId;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.point = point;
    }

    

     public int getCusId() {
        return cusId;
    }

    public void setCusId(int cusId) {
        this.cusId = cusId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }
    
@Override
    public String toString() {
        return "Customer{" + "cusId=" + cusId + ", name=" + name + ", surname=" + surname + ", phone=" + phone + ", point=" + point + '}';
    }
}
