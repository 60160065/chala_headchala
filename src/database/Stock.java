/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author TeddyBank
 */
public class Stock {
    int productId;
    String name;
    String storehouse;
    int amount;
    double price;
    double total;

    public Stock(int productId, String name, String storehouse, int amount, double price, double total) {
        this.productId = productId;
        this.name = name;
        this.storehouse = storehouse;
        this.amount = amount;
        this.price = price;
        this.total = total;
    }
    public Stock() {
        this.productId = -1;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStorehouse() {
        return storehouse;
    }

    public void setStorehouse(String storehouse) {
        this.storehouse = storehouse;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Stock{" + "productId=" + productId + ", name=" + name + ", storehouse=" + storehouse + ", amount=" + amount + ", price=" + price + ", total=" + total + '}';
    }
    
    

    


    
}
