/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chala;

import database.Bill;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author HP
 */
public class PrintBillTableModel extends AbstractTableModel{
    ArrayList<Bill> bill = new ArrayList<Bill>();
    String[] columnNames = {"เครื่องดื่ม","ท็อปปิ้ง","จำนวน","ราคา","จำนวนท็อปปิ้ง","ราคาท็อปปิ้ง","ราคารวม"};

    @Override
    public int getRowCount() {
        return bill.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Bill pb = bill.get(rowIndex);
        switch(columnIndex){
            case 0: return pb.getDrink();
            case 1: return pb.getTopping();
             case 2: return pb.getAmount();
            case 3: return pb.getPrice();
            case 4: return pb.getAmountTopping();
            case 5: return pb.getPriceTopping();
            case 6: return pb.getPrice()+pb.getPriceTopping();
        }
        return "";
    }
    
    public void setData (ArrayList<Bill> PrintBill){
    this.bill= PrintBill;
    fireTableDataChanged();
}

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
    
    
}
