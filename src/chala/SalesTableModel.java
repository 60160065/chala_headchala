/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chala;

import database.Sales;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author informatics
 */
public class SalesTableModel extends AbstractTableModel{
    ArrayList<Sales> sales = new ArrayList<Sales>();
    String[] columnNames = {"วันที่","รายการ","จำนวน","ราคาหน่วย","จำนวนเงิน"};

    @Override
    public int getRowCount() {
        return sales.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Sales s = sales.get(rowIndex);
        switch (columnIndex){
            case 0: return s.getDate();
            case 1: return s.getName()+s.getTopping();
            case 2: return s.getAmount()+s.getAmountTopping();
            case 3: return s.getPrice()+s.getPriceTopping();
            case 4: return (s.getPrice()+s.getPriceTopping());
        }
        return " ";
    }
    
    public void setData(ArrayList<Sales> sales){
        this.sales = sales;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

}
