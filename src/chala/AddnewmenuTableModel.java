/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chala;

import database.Addnewmenu;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author informatics
 */
public class AddnewmenuTableModel extends AbstractTableModel{
    ArrayList<Addnewmenu> menuList = new ArrayList<Addnewmenu>();
    String[] columnName = {"รหัสสินค้า","ชื่อสินค้า","ประเภท","ราคา"};
    @Override
    public int getRowCount() {
        return menuList.size();
    }

    @Override
    public int getColumnCount() {
        return columnName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Addnewmenu addnewmenu = menuList.get(rowIndex);
        switch(columnIndex){
           
            case 0: return addnewmenu.getTypeProductId();
            case 1: return addnewmenu.getName();
            case 2: return addnewmenu.getType();
            case 3: return addnewmenu.getPrice();
            
            
        }
        return "";
    }
    public void setData(ArrayList<Addnewmenu> menuList){
        this.menuList = menuList;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnName[column];
    }
    
    
    
}
