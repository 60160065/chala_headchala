/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chala;

import database.Stock;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author TeddyBank
 */
public class StockTableModel extends AbstractTableModel{
    
    ArrayList<Stock> stock = new ArrayList<Stock>();
    String[] columNames = {"รหัสสินค้า","ชื่อสินค้า","โกดังเก็บ","จำนวน(ชิ้น)","ราคา/หน่วย","จำนวนเงิน(บาท)"};
    @Override
    public int getRowCount() {
        return stock.size();
    }

    @Override
    public int getColumnCount() {
        return columNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Stock lp =stock.get(rowIndex);
        switch(columnIndex){
            case 0:
                return lp.getProductId();
            case 1:
                return lp.getName();
            case 2:
                return lp.getStorehouse();
            case 3:
                return lp.getAmount();
            case 4:
                return lp.getPrice();        
            case 5:
                return lp.getTotal();
        }
        
        return "";
        
    }
    public void setData(ArrayList<Stock> stock){
        this.stock =stock;
        fireTableDataChanged();
    }
    
    @Override
    public String getColumnName(int column) {
        return columNames[column];
    }
            
    
}
