/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chala;

import database.Customer;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author informatics
 */
public class CustomerTableModel extends AbstractTableModel{

    ArrayList<Customer> customerList = new ArrayList<Customer>();
    String[] columnName = {"รหัสลูกค้า","ชื่อ","นามสกุล","เบอร์โทร","แต้มสะสม"};
    @Override
    public int getRowCount() {
        return customerList.size();
    }

    @Override
    public int getColumnCount() {
        return columnName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Customer cus = customerList.get(rowIndex);
        switch(columnIndex){
            case 0: return cus.getCusId();
            case 1: return cus.getName();
            case 2: return cus.getSurname();
            case 3: return cus.getPhone();
            case 4: return cus.getPoint();
        }
        return "";
    }
    
    public void setData(ArrayList<Customer> customerList){
        this.customerList = customerList;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnName[column];
    }
    
    
    
}
