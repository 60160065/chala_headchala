/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chala;

import database.StockDao;
import database.Stock;
import chala.StockTableModel;
import java.awt.Component;
enum FormStateStock{
    INIT,
    ADDNEW,
    UPDATE
}

/**
 *
 * @author Windows 10
 */
public class StockPanel extends javax.swing.JPanel {


    /**
     * Creates new form StockPanel
     */
    public StockPanel() {
        initComponents();
        setState(FormState.INIT);
        updateTable();
        sumPrice();
    }
    private void updateTable(){
        stockTableModel.setData(StockDao.getStock());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        FormPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        dataTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        ProductIDText = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        NameText = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        AmountText = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        StorehouseText = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        PriceText = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        SumText = new javax.swing.JTextField();
        TotalText = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        AddProduct = new javax.swing.JButton();
        SaveProduct = new javax.swing.JButton();
        DelProduct = new javax.swing.JButton();
        Cencel = new javax.swing.JButton();

        FormPanel.setBackground(new java.awt.Color(255, 255, 204));
        FormPanel.setPreferredSize(new java.awt.Dimension(960, 0));

        dataTable.setModel(stockTableModel);
        dataTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dataTableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(dataTable);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("รายการสินค้า");

        ProductIDText.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        ProductIDText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ProductIDTextActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel5.setText("รหัสสินค้า  :");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel6.setText("ชื่อสินค้า :");

        NameText.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        NameText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NameTextActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel7.setText("จำนวน :");

        AmountText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AmountTextActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel8.setText("โกดังเก็บ :");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel9.setText("ราคา/หน่วย :");

        PriceText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PriceTextActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel10.setText("จำนวนเงิน  :");

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel14.setText("รวม : ");

        SumText.setEditable(false);
        SumText.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        SumText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SumTextActionPerformed(evt);
            }
        });

        TotalText.setEditable(false);
        TotalText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TotalTextActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(204, 204, 255));

        AddProduct.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        AddProduct.setText("เพิ่มสินค้า");
        AddProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddProductActionPerformed(evt);
            }
        });

        SaveProduct.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        SaveProduct.setText("บันทึก");
        SaveProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaveProductActionPerformed(evt);
            }
        });

        DelProduct.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        DelProduct.setText("ลบสินค้า");
        DelProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DelProductActionPerformed(evt);
            }
        });

        Cencel.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        Cencel.setText("ยกเลิก");
        Cencel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CencelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(AddProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(SaveProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addComponent(DelProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(Cencel, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(282, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(AddProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SaveProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(DelProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Cencel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(28, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout FormPanelLayout = new javax.swing.GroupLayout(FormPanel);
        FormPanel.setLayout(FormPanelLayout);
        FormPanelLayout.setHorizontalGroup(
            FormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormPanelLayout.createSequentialGroup()
                .addGap(420, 420, 420)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jScrollPane2)
            .addGroup(FormPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(65, 65, 65)
                .addComponent(SumText, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
            .addGroup(FormPanelLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addGroup(FormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(NameText, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ProductIDText, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(FormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(AmountText, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(StorehouseText, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(49, 49, 49)
                .addGroup(FormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(TotalText, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PriceText, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(130, 130, 130))
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        FormPanelLayout.setVerticalGroup(
            FormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormPanelLayout.createSequentialGroup()
                .addGroup(FormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormPanelLayout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addGroup(FormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(PriceText, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(FormPanelLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(FormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ProductIDText, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7)
                            .addComponent(AmountText, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                        .addGroup(FormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(NameText, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8)
                            .addComponent(StorehouseText, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10)
                            .addComponent(TotalText, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(44, 44, 44)))
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 390, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addGroup(FormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(SumText, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jScrollPane1.setViewportView(FormPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 950, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void ProductIDTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ProductIDTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ProductIDTextActionPerformed

    private void NameTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NameTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NameTextActionPerformed

    private void SumTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SumTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_SumTextActionPerformed

    private void dataTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dataTableMouseClicked
     sumPrice();
        //1.หา id
     String idStr = dataTable.getValueAt(dataTable.getSelectedRow(), 0).toString();
     int id = Integer.parseInt(idStr);
    //2.ดึง user จาก db ด้วย id
     stock = StockDao.getStock(id);
    //3.เอา user ไปใส่ form
     toForm(stock);
    }//GEN-LAST:event_dataTableMouseClicked

    private void DelProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DelProductActionPerformed
        setState(FormState.INIT);
        StockDao.delete(stock);
        updateTable();
        clearForm();
    }//GEN-LAST:event_DelProductActionPerformed

    private void AddProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddProductActionPerformed
        setState(FormState.INIT);
        stock = new Stock();
        TotalSet();
        TotalText.setText(""+total);
    }//GEN-LAST:event_AddProductActionPerformed

    private void SaveProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveProductActionPerformed
        setState(FormState.INIT);
        formToObject();
        StockDao.save(stock);
        updateTable();
        clearForm();
        
        
    }//GEN-LAST:event_SaveProductActionPerformed

    private void CencelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CencelActionPerformed
        setState(FormState.ADDNEW);
        clearForm();
    }//GEN-LAST:event_CencelActionPerformed

    private void TotalTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TotalTextActionPerformed
  
    }//GEN-LAST:event_TotalTextActionPerformed

    private void AmountTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AmountTextActionPerformed
     
    }//GEN-LAST:event_AmountTextActionPerformed

    private void PriceTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PriceTextActionPerformed
    
    }//GEN-LAST:event_PriceTextActionPerformed

    private Stock stock;
    StockTableModel stockTableModel = new StockTableModel();
    double currentTotal = 0;
    double sum = 0;
    double total =0;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AddProduct;
    private javax.swing.JTextField AmountText;
    private javax.swing.JButton Cencel;
    private javax.swing.JButton DelProduct;
    private javax.swing.JPanel FormPanel;
    private javax.swing.JTextField NameText;
    private javax.swing.JTextField PriceText;
    private javax.swing.JTextField ProductIDText;
    private javax.swing.JButton SaveProduct;
    private javax.swing.JTextField StorehouseText;
    private javax.swing.JTextField SumText;
    private javax.swing.JTextField TotalText;
    private javax.swing.JTable dataTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables
    private void toForm(Stock stock) {
        ProductIDText.setText(""+stock.getProductId());
        NameText.setText(""+stock.getName());
        StorehouseText.setText(""+stock.getStorehouse());
        PriceText.setText(""+ stock.getPrice());
        AmountText.setText(""+stock.getAmount());
        TotalText.setText(""+stock.getPrice()*stock.getAmount());
        SumText.setText(""+currentTotal);
        
        
    }
    private void formToObject(){
        stock.setName(NameText.getText());
        stock.setStorehouse(StorehouseText.getText());
        String idStr2=AmountText.getText();
        int id2 =Integer.parseInt(idStr2);
        stock.setAmount(id2);
        String idStr =PriceText.getText();
        double id = Double.parseDouble(idStr);
        stock.setPrice(id);
        String idStr3 =TotalText.getText();
        double id3 =Double.parseDouble(idStr3);
        stock.setTotal(id3);
        
        
    }
    private void clearForm(){
        ProductIDText.setText("");
        NameText.setText("");
        StorehouseText.setText("");
        PriceText.setText("");
        AmountText.setText("");
        TotalText.setText("");
        SumText.setText("");
    }
        private void setState(FormState state){
      switch(state){
          case INIT:
              AddProduct.setEnabled(true);
              SaveProduct.setEnabled(true);
              DelProduct.setEnabled(true);
              Cencel.setEnabled(false);
              setFormstate(true);
                  break;
          case ADDNEW:
              AddProduct.setEnabled(false);
              SaveProduct.setEnabled(true);
              DelProduct.setEnabled(false);
              Cencel.setEnabled(true);
              setFormstate(true);
              break;
          case UPDATE:
              AddProduct.setEnabled(false);
              SaveProduct.setEnabled(true);
              DelProduct.setEnabled(true);
              Cencel.setEnabled(true);
              setFormstate(true);
              break;
      }  
    }
 private void setFormstate(boolean state) {
        for(Component c: FormPanel.getComponents() ){
            c.setEnabled(state);
        }
    }
private void sumPrice() {
        for (int i = 0; i < dataTable.getRowCount(); i++) {
            sum += Double.parseDouble(dataTable.getValueAt(i, 5).toString());
        }
        SumText.setText(Double.toString(sum));
        currentTotal = sum;
        sum = 0;
    }
    private void TotalSet(){
        String idStr2=AmountText.getText();
        int id2 =Integer.parseInt(idStr2);
        String idStr =PriceText.getText();
        double id = Double.parseDouble(idStr);
        total =id2*id;
    }

 

}
