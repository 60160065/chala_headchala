/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chala;

/**
 *
 * @author Windows 10
 */
/*POJO*/
public class Members {
   int cusId;
   String name;
   String surname;
   String phone;
   int score;

    public Members(int cusId, String name, String surname, String phone, int score) {
        this.cusId = cusId;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.score = score;
    }

    public int getCusId() {
        return cusId;
    }

    public void setCusId(int cusId) {
        this.cusId = cusId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Members{" + "cusId=" + cusId + ", name=" + name + ", surname=" + surname + ", phone=" + phone + ", score=" + score + '}';
    }

   
   
}
