/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chala;


import database.OrderingVendor;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author PC-Rom
 */
public class OrderingVendorTableModel extends AbstractTableModel {
    ArrayList<OrderingVendor> orderingvendor = new ArrayList<OrderingVendor>();
    String[] columnNames = {"รหัสหุ้นส่วน","รหัสสินค้า","ชื่อสินค้า","จำนวน(ชิ้น)","ราคา/หน่วย","จำนวนเงิน(บาท)","ประเภทร้าน","เบอร์โทรหุ้นส่วน"};
    
    @Override
    public int getRowCount() {
        return orderingvendor.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
       OrderingVendor lp = orderingvendor.get(rowIndex);
       switch(columnIndex){
           case 0: return lp.getVendorId();
           case 1: return lp.getProductId();
           case 2: return lp.getName();
           case 3: return lp.getAmount();
           case 4: return lp.getPrice();
           case 5: return lp.getAmount()*lp.getPrice();
           case 6: return lp.getType();
           case 7: return lp.getPhone();
       }
       return "";
    }
    
    public void setData(ArrayList<OrderingVendor> orderingvendor){
        this.orderingvendor = orderingvendor;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
    
}
